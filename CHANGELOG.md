# Change Log

All notable changes to the "timu-macos-vscode" extension will be documented in this file.

## [1.0.0] - 2023-01-14

- Initial release

## [1.0.1] - 2024-09-28

- Adjust Background Colour in the dark variant.
- Adjust Foreground Colour in the light variant.

## [1.0.2] - 2024-09-28

- Adjust alternative Background Colour in the dark variant.
- Adjust alternative Foreground Colour in the light variant.
