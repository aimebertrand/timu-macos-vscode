# README
## Timu macOS Theme
Color theme inspired by the macOS UI.

<p align="center"><img src="tmv-dark.png" width="100%"/></p>

<p align="center"><img src="tmv-light.png" width="100%"/></p>

## Installation
1. Press extensions icon in your editor.
2. Search for `aimebertrand.timu-macos-vscode`.
3. Select this extension and press `install` button.

## Manual Installation
### VSCodium
Clone this Repo into your extensions directory:

- **Windows:**  `%USERPROFILE%\.vscode-sso\extensions`
- **macOS:**  `~/.vscode-sso/extensions`
- **Linux:**  `~/.vscode-sso/extensions`

### VSCode
Clone this Repo into your extensions directory:

- **Windows:**  `%USERPROFILE%\.vscode\extensions`
- **macOS:**  `~/.vscode/extensions`
- **Linux:**  `~/.vscode/extensions`

## License
MIT License
